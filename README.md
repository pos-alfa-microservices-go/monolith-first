# monolith-first

## Pré requisitos

Instalação necessárias:
- Golang [1.17+](https://go.dev/doc/install)
- Docker [18.02.0+](https://docs.docker.com/engine/install/)


## Execução

Execute `go run main.go` para inicializar o serviço.

```
emmanuelneri@MacBook-Pro-de-Emmanuel monolith-first % go run main.go
{"level":"info","ts":1649039048.774292,"caller":"monolith-first/main.go:11","msg":"Starting monolith..."}
{"level":"info","ts":1649039048.8241239,"caller":"http/server.go:27","msg":"starting httpserver on port: :8080"}

   ____    __
  / __/___/ /  ___
 / _// __/ _ \/ _ \
/___/\__/_//_/\___/ v4.6.3
High performance, minimalist Go web framework
https://echo.labstack.com
____________________________________O/_______
                                    O\
⇨ http server started on [::]:8080
```

## Estrutura do projeto

O projeto é separados em camadas, como demonstrado na imagem abaixo:

![Estrutura do Projeto](./monolith-project-structure.png)

- main.go: Faz o carregamento das configurações e inicializa o serviço;
- http: Define as portas de entradas do server HTTP, com as definições de rotas expostas;
- container: Centraliza e gerência a inicialização das camadas;
- handler: Define a execução de uma rota;
- service: Implementa as execuções dos casos de uso;
- repository: Cadama de comunicação com o banco de dados;
- model: Modelo de dados das entidades;

## Casos de Uso

![Casos de uso](./use-cases.png)

