package service

import (
	"context"
	customErrors "monolith/internal/errors"
	"monolith/internal/repository"

	"monolith/pkg/model"
	"time"
)

type OrderService interface {
	FindById(context.Context, string) (*model.Order, error)
	Create(context.Context, *model.Order) (*model.Order, error)
}

type OrderServiceImpl struct {
	repository      repository.OrderRepository
	customerService CustomerService
	productService  ProductService
}

func NewOrderServiceImpl(repository repository.OrderRepository, customerService CustomerService, productService ProductService) OrderService {
	return &OrderServiceImpl{
		repository:      repository,
		customerService: customerService,
		productService:  productService,
	}
}

func (r OrderServiceImpl) FindById(ctx context.Context, id string) (*model.Order, error) {
	if id == "" {
		return nil, customErrors.ErrEmptyIdParam
	}

	return r.repository.FindById(ctx, id)
}

func (r OrderServiceImpl) Create(ctx context.Context, order *model.Order) (*model.Order, error) {
	if err := order.ValidateToCreate(); err != nil {
		return nil, &customErrors.ValidationError{Message: err.Error()}
	}

	customer, err := r.customerService.FindById(ctx, order.Customer.Id.String())
	if err != nil {
		return nil, err
	}

	if customer == nil {
		return nil, &customErrors.ValidationError{Message: "invalid customer for id " + order.Customer.Id.String()}
	}

	productIds := make([]string, 0)
	for _, i := range order.Items {
		productIds = append(productIds, i.Product.Id.String())
	}

	productById, err := r.productService.FindByIds(ctx, productIds)
	if err != nil {
		return nil, err
	}

	for _, i := range order.Items {
		product := productById[i.Product.Id.String()]
		if product == nil {
			return nil, &customErrors.ValidationError{Message: "invalid product " + i.Product.Id.String()}
		}
	}

	if order.DateTime == nil {
		now := time.Now()
		order.DateTime = &now
	}

	order.Customer = customer
	order.Status = model.CLOSED
	order.CalcTotal()
	return r.repository.Create(ctx, order)
}
