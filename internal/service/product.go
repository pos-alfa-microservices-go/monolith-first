package service

import (
	"context"
	customErrors "monolith/internal/errors"
	"monolith/internal/repository"
	"monolith/pkg/model"
)

type ProductService interface {
	FindAll(context.Context) ([]*model.Product, error)
	FindById(context.Context, string) (*model.Product, error)
	FindByIds(context.Context, []string) (map[string]*model.Product, error)
	Create(context.Context, *model.Product) (*model.Product, error)
}

type ProductServiceImpl struct {
	repository repository.ProductRepository
}

func NewProductServiceImpl(repository repository.ProductRepository) ProductService {
	return &ProductServiceImpl{
		repository: repository,
	}
}

func (s ProductServiceImpl) FindAll(ctx context.Context) ([]*model.Product, error) {
	return s.repository.FindAll(ctx)
}

func (s ProductServiceImpl) FindById(ctx context.Context, id string) (*model.Product, error) {
	if id == "" {
		return nil, customErrors.ErrEmptyIdParam
	}

	return s.repository.FindById(ctx, id)
}

func (s ProductServiceImpl) Create(ctx context.Context, product *model.Product) (*model.Product, error) {
	return s.repository.Create(ctx, product)
}

func (s ProductServiceImpl) FindByIds(ctx context.Context, ids []string) (map[string]*model.Product, error) {
	if len(ids) == 0 {
		return nil, customErrors.ErrEmptyIdParam
	}

	products, err := s.repository.FindByIds(ctx, ids)
	if err != nil {
		return nil, err
	}

	productById := make(map[string]*model.Product, 0)
	for _, p := range products {
		productById[p.Id.String()] = p
	}

	return productById, nil
}
