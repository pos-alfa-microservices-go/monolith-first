package service

import (
	"context"
	customErrors "monolith/internal/errors"
	"monolith/internal/repository"

	"monolith/pkg/model"
)

type CustomerService interface {
	FindAll(context.Context) ([]*model.Customer, error)
	FindById(context.Context, string) (*model.Customer, error)
	Create(context.Context, *model.Customer) (*model.Customer, error)
}

type CustomerServiceImpl struct {
	repository  repository.CustomerRepository
	userService UserService
}

func NewCustomerServiceImpl(repository repository.CustomerRepository, userService UserService) CustomerService {
	return &CustomerServiceImpl{
		repository:  repository,
		userService: userService,
	}
}

func (r CustomerServiceImpl) FindAll(ctx context.Context) ([]*model.Customer, error) {
	return r.repository.FindAll(ctx)
}

func (r CustomerServiceImpl) FindById(ctx context.Context, id string) (*model.Customer, error) {
	if id == "" {
		return nil, customErrors.ErrEmptyIdParam
	}

	return r.repository.FindById(ctx, id)
}

func (r CustomerServiceImpl) Create(ctx context.Context, customer *model.Customer) (*model.Customer, error) {
	err := r.repository.RunInTransaction(context.Background(), func(ctx context.Context) error {
		user, err := r.userService.Create(ctx, &model.User{
			Login: customer.Email,
		})

		if err != nil {
			return err
		}

		user.Password = ""
		customer.User = user
		customer, err = r.repository.Create(ctx, customer)

		return err
	})

	if err != nil {
		return nil, err
	}

	return customer, nil
}
