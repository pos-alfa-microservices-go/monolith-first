package service

import (
	"context"
	"fmt"
	"monolith/internal/commons/hash"
	customErrors "monolith/internal/errors"
	"monolith/internal/repository"
	"monolith/pkg/model"
)

const defaultPassaword = "golang"

type UserService interface {
	FindById(context.Context, string) (*model.User, error)
	FindByLogin(context.Context, string) (*model.User, error)
	Create(context.Context, *model.User) (*model.User, error)
}

type UserServiceImpl struct {
	repository repository.UserRepository
}

func NewUserServiceImpl(repository repository.UserRepository) UserService {
	return &UserServiceImpl{
		repository: repository,
	}
}

func (r UserServiceImpl) FindById(ctx context.Context, id string) (*model.User, error) {
	if id == "" {
		return nil, customErrors.ErrEmptyIdParam
	}

	return r.repository.FindById(context.Background(), id)
}

func (r UserServiceImpl) FindByLogin(ctx context.Context, id string) (*model.User, error) {
	if id == "" {
		return nil, customErrors.ErrEmptyIdParam
	}

	return r.repository.FindByLogin(context.Background(), id)
}

func (r UserServiceImpl) Create(ctx context.Context, user *model.User) (*model.User, error) {
	u, err := r.repository.FindByLogin(ctx, user.Login)
	if err != nil {
		return nil, err
	}

	if u != nil {
		return nil, &customErrors.ValidationError{
			Message: fmt.Sprintf("user %s already exists", u.Login),
		}
	}

	hashPassword, err := hash.NewHash(defaultPassaword)
	if err != nil {
		return nil, err
	}

	user.Password = hashPassword

	return r.repository.Create(ctx, user)
}
