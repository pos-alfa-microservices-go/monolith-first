package handler

import (
	"context"
	"monolith/internal/service"
	"monolith/pkg/model"
	"net/http"

	"github.com/labstack/echo/v4"
)

type ProductHandler interface {
	GetAll(c echo.Context) error
	GetById(c echo.Context) error
	Create(c echo.Context) error
}

type HttpProductHandler struct {
	productService service.ProductService
}

func NewProductHandler(productService service.ProductService) ProductHandler {
	return &HttpProductHandler{
		productService: productService,
	}
}

func (h HttpProductHandler) GetAll(c echo.Context) error {
	products, err := h.productService.FindAll(context.Background())
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, products)
}

func (h HttpProductHandler) GetById(c echo.Context) error {
	id := c.Param("id")
	product, err := h.productService.FindById(context.Background(), id)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, product)
}

func (h HttpProductHandler) Create(c echo.Context) error {
	productRequest := model.Product{}
	if err := c.Bind(&productRequest); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	product, err := h.productService.Create(context.Background(), &productRequest)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, product)
}
