package handler

import (
	"context"
	"monolith/internal/service"
	"monolith/pkg/model"
	"net/http"

	"github.com/labstack/echo/v4"
)

type CustomerHandler interface {
	GetAll(c echo.Context) error
	GetById(c echo.Context) error
	Create(c echo.Context) error
}

type HttpCustomerHandler struct {
	CustomerService service.CustomerService
}

func NewCustomerHandler(CustomerService service.CustomerService) CustomerHandler {
	return &HttpCustomerHandler{
		CustomerService: CustomerService,
	}
}

func (h HttpCustomerHandler) GetAll(c echo.Context) error {
	customers, err := h.CustomerService.FindAll(context.Background())
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, customers)
}

func (h HttpCustomerHandler) GetById(c echo.Context) error {
	id := c.Param("id")
	customer, err := h.CustomerService.FindById(context.Background(), id)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, customer)
}

func (h HttpCustomerHandler) Create(c echo.Context) error {
	customerRequest := model.Customer{}
	if err := c.Bind(&customerRequest); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	customer, err := h.CustomerService.Create(context.Background(), &customerRequest)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, customer)
}
