package handler

import (
	"context"
	"monolith/internal/service"
	"monolith/pkg/model"
	"net/http"

	"github.com/labstack/echo/v4"
)

type OrderHandler interface {
	GetById(c echo.Context) error
	Create(c echo.Context) error
}

type HttpOrderHandler struct {
	orderService service.OrderService
}

func NewHttpOrderHandler(orderService service.OrderService) OrderHandler {
	return &HttpOrderHandler{
		orderService: orderService,
	}
}

func (h HttpOrderHandler) GetById(c echo.Context) error {
	id := c.Param("id")
	order, err := h.orderService.FindById(context.Background(), id)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, order)
}

func (h HttpOrderHandler) Create(c echo.Context) error {
	orderRequest := model.Order{}
	if err := c.Bind(&orderRequest); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}

	order, err := h.orderService.Create(context.Background(), &orderRequest)
	if err != nil {
		return err
	}

	return c.JSON(http.StatusOK, order)
}
