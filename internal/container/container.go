package container

import (
	"monolith/internal/auth"
	"monolith/internal/config"
	"monolith/internal/database"
	"monolith/internal/repository"
	"monolith/internal/service"
)

type Container struct {
	AppConfig *config.AppConfig

	OrderService    service.OrderService
	ProductService  service.ProductService
	CustomerService service.CustomerService
	UserService     service.UserService

	AuthManager auth.AuthManager
}

func NewContainer(appConfig *config.AppConfig) *Container {
	return &Container{
		AppConfig: appConfig,
	}
}

func (c *Container) Start() error {
	pool, err := config.StartPool(c.AppConfig)
	if err != nil {
		return err
	}

	databaseManager := database.NewDatabaseManagerImpl(pool)

	customerRepository := repository.NewDBCustomerRepository(databaseManager)
	userRepository := repository.NewDBUserRepository(databaseManager)
	productRepository := repository.NewDBProductRepository(databaseManager)
	orderRepository := repository.NewDBOrderRepository(databaseManager)

	c.ProductService = service.NewProductServiceImpl(productRepository)
	c.UserService = service.NewUserServiceImpl(userRepository)
	c.CustomerService = service.NewCustomerServiceImpl(customerRepository, c.UserService)
	c.OrderService = service.NewOrderServiceImpl(orderRepository, c.CustomerService, c.ProductService)

	c.AuthManager = auth.NewAuthJWT(c.AppConfig, c.UserService)

	return nil
}
