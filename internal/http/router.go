package http

import (
	"encoding/json"
	"fmt"
	"monolith/internal/container"
	"monolith/internal/handler"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

type Router interface {
	Create() *echo.Echo
}

type ServerRouter struct {
	container *container.Container
}

func NewRouter(c *container.Container) Router {
	return &ServerRouter{
		container: c,
	}
}

func (r *ServerRouter) Create() *echo.Echo {
	e := echo.New()
	e.Use(middleware.Logger())

	userLogadedMiddleware := middleware.JWT([]byte(r.container.AppConfig.JWT.Secret))

	errorHandler := NewCustomErrorHandler(e)
	e.HTTPErrorHandler = errorHandler.ErrorHandler

	authHandler := handler.NewAuthHandler(r.container.AuthManager)

	orderHandler := handler.NewHttpOrderHandler(r.container.OrderService)
	productHandler := handler.NewProductHandler(r.container.ProductService)
	customerHandler := handler.NewCustomerHandler(r.container.CustomerService)

	e.POST("/login", authHandler.Login)

	orders := e.Group("/orders")
	orders.GET("/:id", orderHandler.GetById, userLogadedMiddleware)
	orders.POST("", orderHandler.Create, userLogadedMiddleware)

	products := e.Group("/products")
	products.GET("", productHandler.GetAll)
	products.GET("/:id", productHandler.GetById)
	products.POST("", productHandler.Create, userLogadedMiddleware)

	customers := e.Group("/customers")
	customers.GET("", customerHandler.GetAll, userLogadedMiddleware)
	customers.GET("/:id", customerHandler.GetById, userLogadedMiddleware)
	customers.POST("", customerHandler.Create)

	printRoutes(e)

	return e
}

func printRoutes(e *echo.Echo) {
	data, err := json.MarshalIndent(e.Routes(), "", "  ")
	if err != nil {
		panic(err)
	}

	fmt.Println(string(data))
}
