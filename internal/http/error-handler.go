package http

import (
	"errors"
	customErrors "monolith/internal/errors"
	"net/http"

	"github.com/labstack/echo/v4"
)

type CustomErrorHandler struct {
	e *echo.Echo
}

func NewCustomErrorHandler(e *echo.Echo) *CustomErrorHandler {
	return &CustomErrorHandler{e: e}
}

func (h *CustomErrorHandler) ErrorHandler(err error, c echo.Context) {
	if c.Response().Committed {
		return
	}

	if errors.Is(err, customErrors.ErrInvalidUser) {
		c.JSON(http.StatusUnauthorized, "")
		return
	}

	if errors.Is(err, customErrors.ErrEmptyIdParam) {
		message := map[string]string{"message": customErrors.ErrEmptyIdParam.Error()}
		c.JSON(http.StatusBadRequest, message)
		return
	}

	if ve, ok := err.(*customErrors.ValidationError); ok {
		message := map[string]string{"message": ve.Message}
		c.JSON(http.StatusBadRequest, message)
		return
	}

	if ic, ok := err.(*customErrors.InvalidConstraint); ok {
		message := map[string]string{"message": ic.Message}
		c.JSON(http.StatusBadRequest, message)
		return
	}

	if de, ok := err.(*customErrors.DatabaseError); ok {
		message := map[string]string{"message": de.Message}
		c.JSON(http.StatusBadRequest, message)
		return
	}

	if br, ok := err.(*customErrors.BadRequestError); ok {
		message := map[string]string{"message": br.Message}
		c.JSON(http.StatusBadRequest, message)
		return
	}

	h.e.DefaultHTTPErrorHandler(err, c)
}
