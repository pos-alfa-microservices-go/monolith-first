package repository

import (
	"context"
	"errors"
	"monolith/internal/database"
	"monolith/pkg/model"

	"github.com/google/uuid"
	"github.com/jackc/pgx/v4"
)

type UserRepository interface {
	FindById(context.Context, string) (*model.User, error)
	FindByLogin(context.Context, string) (*model.User, error)
	Create(context.Context, *model.User) (*model.User, error)
}

type DBUserRepository struct {
	databaseManager database.DatabaseManager
}

func NewDBUserRepository(databaseManager database.DatabaseManager) UserRepository {
	return &DBUserRepository{databaseManager: databaseManager}
}

func (r DBUserRepository) Create(ctx context.Context, user *model.User) (*model.User, error) {
	sql := "insert into \"user\" (login, password) values ($1, $2) returning id"

	var id uuid.UUID
	err := r.databaseManager.QueryRow(ctx, sql, user.Login, user.Password).Scan(&id)

	if err != nil {
		return nil, err
	}

	user.Id = id

	return user, nil
}

func (r DBUserRepository) FindById(ctx context.Context, id string) (*model.User, error) {
	return r.findBy(ctx, "id", id, false)

}

func (r DBUserRepository) FindByLogin(ctx context.Context, login string) (*model.User, error) {
	return r.findBy(ctx, "login", login, true)
}

func (r DBUserRepository) findBy(ctx context.Context, paramName, paramValue string, withPassword bool) (*model.User, error) {
	sql := "select id, login, password from \"user\" u where " + paramName + " = $1"

	var id uuid.UUID
	var login string
	var password string

	err := r.databaseManager.QueryRow(ctx, sql, paramValue).Scan(&id, &login, &password)
	if err != nil {
		if errors.Is(err, pgx.ErrNoRows) {
			return nil, nil
		}

		return nil, err
	}

	user := &model.User{
		Id:    id,
		Login: login,
	}

	if withPassword {
		user.Password = password
	}

	return user, nil
}
