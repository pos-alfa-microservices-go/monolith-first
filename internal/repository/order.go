package repository

import (
	"context"
	"monolith/internal/database"
	"monolith/pkg/model"
	"time"

	"github.com/google/uuid"
	pgtypeExt "github.com/jackc/pgtype/ext/shopspring-numeric"
	"github.com/jackc/pgx/v4"
	"github.com/shopspring/decimal"
)

const (
	selectItemsByOrderId = "select oi.id, quantity, unit_value, total, p.id, p.name from order_item oi inner join product p on p.id = oi.product_id where order_id = $1;"
)

type OrderRepository interface {
	FindById(context.Context, string) (*model.Order, error)
	Create(context.Context, *model.Order) (*model.Order, error)
	UpdateStatus() error
}

type DBOrderRepository struct {
	databaseManager database.DatabaseManager
}

func NewDBOrderRepository(databaseManager database.DatabaseManager) OrderRepository {
	return &DBOrderRepository{databaseManager: databaseManager}
}

func (r DBOrderRepository) FindById(ctx context.Context, id string) (*model.Order, error) {
	sql := "select o.id, date_time, status, total, c.id, c.name, c.email from \"order\" o inner join customer c on c.id = o.customer_id where o.id = $1"
	return r.findBy(ctx, sql, id)
}

func (r DBOrderRepository) findBy(ctx context.Context, sql string, arguments ...interface{}) (*model.Order, error) {
	var id uuid.UUID
	var dateTime time.Time
	var status string
	var total pgtypeExt.Numeric
	var customerId uuid.UUID
	var customerName string
	var customerEmail string

	err := r.databaseManager.QueryRow(ctx, sql, arguments...).Scan(&id, &dateTime, &status, &total, &customerId, &customerName, &customerEmail)
	if err != nil {
		return nil, err
	}

	order := &model.Order{
		Id:       id,
		DateTime: &dateTime,
		Status:   model.OrderStatusMap[status],
		Total:    total.Decimal,
		Customer: &model.Customer{
			Id:    customerId,
			Name:  customerName,
			Email: customerEmail,
		},
	}

	rows, err := r.databaseManager.Query(ctx, selectItemsByOrderId, id)
	if err != nil {
		return nil, err
	}

	items, err := readOrderItems(rows)
	if err != nil {
		return nil, err
	}

	order.Items = items

	return order, nil
}

func (r DBOrderRepository) UpdateStatus() error {
	return nil
}

func (r DBOrderRepository) Create(ctx context.Context, order *model.Order) (*model.Order, error) {
	err := r.databaseManager.RunInTransaction(ctx, func(ctx context.Context) error {
		order, err := r.createOrder(ctx, order)
		if err != nil {
			return err
		}

		items, err := r.createOrderItems(ctx, order)
		if err != nil {
			return err
		}

		order.Items = items

		return nil
	})

	if err != nil {
		return nil, err
	}

	return order, nil
}

func (r DBOrderRepository) createOrder(ctx context.Context, order *model.Order) (*model.Order, error) {
	sql := "insert into \"order\" (customer_id, date_time, status, total) values ($1, $2, $3, $4) returning id;"
	var orderId uuid.UUID
	err := r.databaseManager.QueryRow(ctx, sql,
		order.Customer.Id, order.DateTime, order.Status.String(), order.Total).Scan(&orderId)

	if err != nil {
		return nil, err
	}

	order.Id = orderId

	return order, nil
}

func (r DBOrderRepository) createOrderItems(ctx context.Context, order *model.Order) ([]*model.OrderItem, error) {
	queries := make([]database.BatchQuery, 0)
	sql := "insert into order_item (order_id, product_id, quantity, unit_value, total) values ($1, $2, $3, $4, $5);"
	for _, i := range order.Items {
		queries = append(queries, database.BatchQuery{
			Sql: sql,
			Args: []interface{}{
				order.Id,
				i.Product.Id,
				i.Quantity,
				i.UnitValue,
				i.Total,
			},
		})
	}

	queries = append(queries, database.BatchQuery{
		Sql:  selectItemsByOrderId,
		Args: []interface{}{order.Id},
	})

	result, err := r.databaseManager.Batch(ctx, queries)
	if err != nil {
		return nil, err
	}

	defer result.Close()
	for i := 0; i < len(order.Items); i++ {
		_, err = result.Exec()
		if err != nil {
			return nil, err
		}
	}

	rows, err := result.Query()
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	return readOrderItems(rows)
}

func readOrderItems(rows pgx.Rows) ([]*model.OrderItem, error) {
	items := make([]*model.OrderItem, 0)

	for rows.Next() {
		var id uuid.UUID
		var quantity int32
		var unitValue decimal.Decimal
		var total decimal.Decimal
		var productId uuid.UUID
		var productName string

		if err := rows.Scan(&id, &quantity, &unitValue, &total, &productId, &productName); err != nil {
			return nil, err
		}

		items = append(items, &model.OrderItem{
			Id:        id,
			Quantity:  quantity,
			UnitValue: unitValue,
			Total:     total,
			Product: &model.Product{
				Id:   productId,
				Name: productName,
			},
		})
	}

	return items, nil
}
