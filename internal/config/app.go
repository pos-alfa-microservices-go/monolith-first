package config

type AppConfig struct {
	Server   HTTPServer
	Database Database
	JWT      JWT
}

type HTTPServer struct {
	Port int
}

type Database struct {
	User     string
	Password string
	Host     string
	Port     int
	Name     string
}

type JWT struct {
	Secret string
}
