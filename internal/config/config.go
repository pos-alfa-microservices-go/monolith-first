package config

import "github.com/spf13/viper"

func Start() (*AppConfig, error) {
	viper.SetConfigName("env")
	viper.AddConfigPath("configs")
	viper.SetConfigType("yaml")
	viper.AutomaticEnv()
	if err := viper.ReadInConfig(); err != nil {
		return nil, err
	}

	return &AppConfig{
		Server: HTTPServer{
			Port: viper.GetInt("server.port"),
		},
		Database: Database{
			Host:     viper.GetString("database.host"),
			Port:     viper.GetInt("database.port"),
			Name:     viper.GetString("database.name"),
			User:     viper.GetString("database.user"),
			Password: viper.GetString("database.password"),
		},
		JWT: JWT{
			Secret: viper.GetString("jwt.secret"),
		},
	}, nil
}
