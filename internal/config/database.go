package config

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v4/pgxpool"
)

func StartPool(appConfig *AppConfig) (*pgxpool.Pool, error) {
	conn := fmt.Sprintf("user=%s password=%s host=%s port=%d dbname=%s",
		appConfig.Database.User, appConfig.Database.Password, appConfig.Database.Host,
		appConfig.Database.Port, appConfig.Database.Name)

	config, err := pgxpool.ParseConfig(conn)
	if err != nil {
		return nil, err
	}

	config.MinConns = 2
	config.MinConns = 10

	return pgxpool.ConnectConfig(context.Background(), config)
}
