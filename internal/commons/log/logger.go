package log

import "go.uber.org/zap"

var Logger *zap.SugaredLogger

func init() {
	Logger = start()
}

func start() *zap.SugaredLogger {
	logger, err := zap.NewProduction()
	if err != nil {
		panic(err)
	}

	defer logger.Sync()

	return logger.Sugar()
}
