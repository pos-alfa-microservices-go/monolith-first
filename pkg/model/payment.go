package model

import (
	"errors"
	"time"

	"github.com/google/uuid"
	"github.com/shopspring/decimal"
)

type PaymentStatus int

const (
	PAYED PaymentStatus = iota
)

type Payment struct {
	Id        uuid.UUID `json:",omitempty"`
	Order     *Order
	CreatedAt time.Time
	PayedAt   *time.Time
	Status    PaymentStatus
	Total     decimal.Decimal
}

func (p Payment) ValidateToCreate() error {
	if p.Order == nil {
		return errors.New("order related is required")
	}

	if p.Total.IsZero() {
		return errors.New("payment must have a total value more then zero")
	}

	return nil
}
