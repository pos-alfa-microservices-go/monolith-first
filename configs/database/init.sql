CREATE TABLE "user"
(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid (),
    login VARCHAR(200) NOT NULL,
    password VARCHAR(72) NOT NULL,
    CONSTRAINT user_uk UNIQUE(login)
);

CREATE TABLE customer
(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid (),
    name VARCHAR(200) NOT NULL,
    email VARCHAR(200) NOT NULL,
    user_id UUID NOT NULL,
    CONSTRAINT customer_user_id FOREIGN KEY (user_id) REFERENCES "user"(id)
);

CREATE TABLE "order"
(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid (),
    customer_id UUID NOT NULL,
    date_time timestamp NOT NULL,
    status VARCHAR(100) NOT NULL,
    total numeric(19, 2) NOT NULL,
    CONSTRAINT order_customer_id FOREIGN KEY (customer_id) REFERENCES customer(id)
);

CREATE TABLE product
(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid (),
    name VARCHAR(200) NOT NULL,
    value numeric(19, 2) NOT NULL
);

CREATE TABLE order_item
(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid (),
    order_id UUID NOT NULL,
    product_id UUID NOT NULL,
    quantity int NOT NULL,
    unit_value numeric(19, 2) NOT NULL,
    total numeric(19, 2) NOT NULL,
    CONSTRAINT order_item_order_id_fk FOREIGN KEY (order_id) REFERENCES "order"(id),
    CONSTRAINT order_item_product_id_fk FOREIGN KEY (product_id) REFERENCES product(id)
);

CREATE TABLE payment
(
    id UUID PRIMARY KEY DEFAULT gen_random_uuid (),
    order_id UUID NOT NULL,
    created_at timestamp NOT NULL,
    payed_at timestamp,
    status VARCHAR(100) NOT NULL,
    total numeric(19, 2) NOT NULL,
    CONSTRAINT payment_customer_id FOREIGN KEY (order_id) REFERENCES "order"(id)
);
