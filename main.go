package main

import (
	"monolith/internal/commons/log"
	"monolith/internal/config"
	"monolith/internal/container"
	"monolith/internal/http"
)

func main() {
	log.Logger.Info("Starting monolith...")

	conf, err := config.Start()
	if err != nil {
		log.Logger.Fatal("", err)
	}

	container := container.NewContainer(conf)
	if err := container.Start(); err != nil {
		log.Logger.Fatal(err)
	}

	r := http.NewRouter(container)
	if err := http.NewHttpServer(conf.Server.Port).Start(r); err != nil {
		log.Logger.Fatal("fail on start httpserver", err)
	}

}
